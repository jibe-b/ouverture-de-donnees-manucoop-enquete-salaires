# Données à ouvrir : enquête sur les salaires par la Manufacture Coopérative

## Description

Les productions scientifiques incluent des données brutes qui gagneront à être mises à disposition afin de permettre des réutilisations et d'éventuels avancées qui n'étaient pas anticipées par les producteurs originaux de données.

Dans le cadre de l'application de la loi République numérique, les données issues de projets de recherche financés par un minimum de 50% d'argent public doivent être mis en ligne en open data.

## Contenus scientifiques (à ouvrir)

L'enquête sur les salaires dans les CAE réalisée par la Manufacture Coopérative (2015) a donné lieu à une publication ("le PDF de l'enquête", [disponible en ligne](http://www.coopaname.coop/sites/www.coopaname.coop/files/file_fields/2018/01/10/enquete-revenu-temps-travail-complet.pdf)) ainsi que des jeux de données collectés pour l'enquête.

## Processus d'ouverture de données

Afin de rendre la publication issue de l'enquête améliorable, il est nécessaire de la convertir en un document exécutable (type notebook jupyter).

Par ailleurs, les jeux de données doivent être enrichis (normalisation du modèle de données) afin de permettre son réemploi.

## Animation du réemploi des données

Les données pourront être réemployées par les coopérateur·rice·s de CAE afin de comparer leur propre situation économique avec celle observée au cours de l'enquête.

Par ailleurs, la Manufacture Coopérative peut bénéficier de cette amélioration des formats de la publication et des jeux de données pour assurer la mise à jour de l'enquête au cours des années.



